package laas;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import laas.domain.request.CustomerApplicationRequest;
import laas.domain.response.LoanStatisticsResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import laas.model.Contract;
import laas.model.Customer;
import laas.model.Manager;
import laas.service.LoanService;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class LoanAmountApprovalServiceApplicationTests {

    private static final String CUSTOMER_ID = "d2-d1s-tok";
    private static final BigDecimal LOAN_AMOUNT = BigDecimal.valueOf(1500);
    private static Contract CONTRACT = Contract.builder().build();
    private static final List<Contract> CONTRACTS = new ArrayList<>();
    private static final String USERNAME1 = "oleska";
    private static final String USERNAME2 = "john";
    private static final String APPROVED = "Application is approved";
    private static final Boolean IS_APPROVED = true;
    private static final String PROCESSING = "Application is processing";
    private static Customer CUSTOMER = Customer.builder()
            .id(CUSTOMER_ID)
            .build();
    private static final List<String> USERNAMES = new ArrayList<>();
    private static List<Manager> MANAGERS = new ArrayList<>();
    private static final Manager MANAGER = Manager.builder()
            .username(USERNAME1)
            .isApproved(IS_APPROVED)
            .build();

    private static final CustomerApplicationRequest REQUEST = CustomerApplicationRequest.builder()
            .customerId(CUSTOMER_ID)
            .loanAmount(LOAN_AMOUNT)
            .usernames(USERNAMES)
            .build();

    private static final LoanStatisticsResponse RESPONSE = LoanStatisticsResponse.builder()
            .contractsCount(2)
            .sumOfAmounts(BigDecimal.valueOf(250))
            .avgAmount(BigDecimal.valueOf(125))
            .maxAmount(BigDecimal.valueOf(150))
            .minAmount(BigDecimal.valueOf(100))
            .build();

    @Mock
    private LoanService loanService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CUSTOMER = Customer.builder().id(CUSTOMER_ID).build();
        CONTRACT = Contract.builder().build();
        MANAGERS = new ArrayList<>();
    }

    @Test
    public void shouldProceedCustomerRequestTest() {
        when(loanService.proceedCustomerRequest(any())).thenReturn(PROCESSING);

        String response = loanService.proceedCustomerRequest(REQUEST);

        assertThat(response, is(notNullValue()));
        assertThat(response, equalTo(PROCESSING));
    }

    @Test
    public void shouldGetManagerApprovalTest() {
        when(loanService.getManagerApproval(anyString(), anyString())).thenReturn(APPROVED);

        String approval = loanService.getManagerApproval(CUSTOMER_ID, USERNAME1);

        assertThat(approval, is(notNullValue()));
        assertThat(approval, equalTo(APPROVED));
    }

    @Test
    public void shouldCheckIfAllManagersApprovedTest() {
        MANAGERS.add(MANAGER);

        Manager manager2 = Manager.builder()
                .username(USERNAME2)
                .isApproved(IS_APPROVED)
                .build();
        MANAGERS.add(manager2);

        when(loanService.isAllManagersApproved(any())).thenReturn(true);

        boolean ifAllApproved = loanService.isAllManagersApproved(MANAGERS);

        assertThat(ifAllApproved, is(notNullValue()));
        assertTrue(ifAllApproved);
    }

    @Test
    public void shouldCheckIfAtLeastOneManagerNotApprovedTest() {
        MANAGERS.add(MANAGER);

        Manager manager2 = Manager.builder()
                .username(USERNAME2)
                .isApproved(!IS_APPROVED)
                .build();
        MANAGERS.add(manager2);

        boolean ifAllApproved = loanService.isAllManagersApproved(MANAGERS);

        assertThat(ifAllApproved, is(notNullValue()));
        assertFalse(ifAllApproved);
    }

    @Test
    public void shouldGetUsernamesFromCustomerRequest() {
        MANAGERS.add(MANAGER);
        USERNAMES.add(USERNAME1);

        when(loanService.getUsernamesFromCustomerRequest(any())).thenReturn(USERNAMES);

        List<String> response = loanService.getUsernamesFromCustomerRequest(MANAGERS);

        assertThat(response, is(notNullValue()));
        assertThat(response.get(0), equalTo(USERNAME1));
    }

    @Test
    public void shouldGetCustomerByIdTest() {
        when(loanService.getCustomerById(anyString())).thenReturn(Optional.of(CUSTOMER));

        Optional<Customer> customer = loanService.getCustomerById(CUSTOMER_ID);

        assertThat(customer, is(notNullValue()));
        assertTrue(customer.isPresent() && customer.get().equals(CUSTOMER));
    }

    @Test
    public void shouldCheckPendingRequestsTest() {
        CUSTOMER.setPendingRequest(true);
        when(loanService.checkForPendingLoanRequest(CUSTOMER)).thenReturn(true);

        boolean isPending = loanService.checkForPendingLoanRequest(CUSTOMER);

        assertTrue(isPending);
    }

    @Test
    public void shouldCreateNewContractTest() {
        CONTRACT.setLoanAmount(LOAN_AMOUNT);
        when(loanService.createNewContract(LOAN_AMOUNT)).thenReturn(CONTRACT);

        Contract contract = loanService.createNewContract(LOAN_AMOUNT);

        assertThat(contract, is(notNullValue()));
        assertThat(contract.getLoanAmount(), equalTo(LOAN_AMOUNT));
    }

    @Test
    public void shouldGetManagersForCustomer() {
        USERNAMES.add(USERNAME1);
        MANAGERS.add(MANAGER);

        when(loanService.getManagersForCustomer(any())).thenReturn(MANAGERS);

        List<Manager> response = loanService.getManagersForCustomer(USERNAMES);

        assertThat(response, is(notNullValue()));
        assertThat(response.get(0).getUsername(), equalTo(USERNAME1));
    }

    @Test
    public void shouldGetCustomerLoanStatisticsTest() {
        CONTRACT.setApprovalSentDateTime(LocalDateTime.now().minusSeconds(15));
        CONTRACT.setLoanAmount(BigDecimal.valueOf(100));
        CONTRACTS.add(CONTRACT);
        CONTRACT.setApprovalSentDateTime(LocalDateTime.now().minusSeconds(30));
        CONTRACT.setLoanAmount(BigDecimal.valueOf(150));
        CONTRACTS.add(CONTRACT);

        CUSTOMER.setContracts(CONTRACTS);

        when(loanService.getCustomerLoanStatistics(any())).thenReturn(RESPONSE);

        LoanStatisticsResponse response = loanService.getCustomerLoanStatistics(CUSTOMER_ID);

        assertThat(response, is(notNullValue()));
        assertThat(response.getContractsCount(), equalTo(2));
    }

    @Test
    public void shouldGetRecentlyApprovedContractsTest() {
        CONTRACT.setApprovalSentDateTime(LocalDateTime.now().minus(30, ChronoUnit.SECONDS));
        Contract recentContract = CONTRACT;
        CONTRACT.setApprovalSentDateTime(LocalDateTime.now().minus(300, ChronoUnit.SECONDS));
        Contract oldContract = CONTRACT;
        CONTRACTS.add(recentContract);
        CONTRACTS.add(oldContract);

        CUSTOMER.setPendingRequest(true);
        CUSTOMER.setContracts(CONTRACTS);

        when(loanService.getRecentlyApprovedContracts(CONTRACTS)).thenReturn(List.of(recentContract));

        List<Contract> contracts = loanService.getRecentlyApprovedContracts(CONTRACTS);

        assertThat(contracts, is(notNullValue()));
        assertThat(contracts.get(0), equalTo(recentContract));
    }

    @Test
    public void shouldNotFindRecentlyApprovedContractsTest() {
        CONTRACT.setApprovalSentDateTime(LocalDateTime.now().minus(300, ChronoUnit.SECONDS));
        CONTRACTS.add(CONTRACT);

        CUSTOMER.setPendingRequest(true);
        CUSTOMER.setContracts(CONTRACTS);

        when(loanService.getRecentlyApprovedContracts(CONTRACTS)).thenReturn(Collections.emptyList());

        List<Contract> contracts = loanService.getRecentlyApprovedContracts(CONTRACTS);

        assertThat(contracts, is(notNullValue()));
        assertTrue(contracts.isEmpty());
    }

}