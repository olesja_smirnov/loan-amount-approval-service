package laas;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import laas.controller.LoanApprovalController;
import laas.domain.request.CustomerApplicationRequest;
import laas.domain.request.ManagerApprovalRequest;
import laas.domain.response.LoanStatisticsResponse;
import laas.service.LoanService;

@RunWith(MockitoJUnitRunner.class)
public class ControllerIntegrationTest {

    private static final String CUSTOMER_ID = "d2-d1s-tok";
    private static final String REQUEST_IS_RECEIVED = "Request is received";
    private static final String APPROVED = "Application is approved";
    private static final BigDecimal LOAN_AMOUNT = BigDecimal.valueOf(1500);
    private static final String USERNAME = "oleska";
    private static final List<String> USERNAMES = new ArrayList<>();

    private static final CustomerApplicationRequest CUSTOMER_REQUEST = CustomerApplicationRequest.builder()
            .customerId(CUSTOMER_ID)
            .loanAmount(LOAN_AMOUNT)
            .usernames(USERNAMES)
            .build();

    private static final ManagerApprovalRequest MANAGER_REQUEST = ManagerApprovalRequest.builder()
            .username(USERNAME)
            .customerId(CUSTOMER_ID)
            .build();

    private static final LoanStatisticsResponse RESPONSE = LoanStatisticsResponse.builder()
            .contractsCount(2)
            .sumOfAmounts(BigDecimal.valueOf(250))
            .avgAmount(BigDecimal.valueOf(125))
            .maxAmount(BigDecimal.valueOf(150))
            .minAmount(BigDecimal.valueOf(100))
            .build();

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @InjectMocks
    private LoanApprovalController controller;

    @Mock
    private LoanService service;

    @Test
    public void shouldSendLoanRequest() {

        when(service.proceedCustomerRequest(any()))
                .thenReturn(REQUEST_IS_RECEIVED);

        ResponseEntity<String> response = controller.approveCustomerLoanRequest(CUSTOMER_REQUEST);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), REQUEST_IS_RECEIVED);
    }

    @Test
    public void shouldReceiveManagerApproval() {

        when(service.getManagerApproval(any(), any()))
                .thenReturn(APPROVED);

        ResponseEntity<String> response = controller.sendRequestToManagers(MANAGER_REQUEST);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(response.getBody(), APPROVED);
    }

    @Test
    public void shouldReturnCustomerLoanStatistics() {

        when(service.getCustomerLoanStatistics(any()))
                .thenReturn(RESPONSE);

        ResponseEntity<LoanStatisticsResponse> responseEntity = controller.receiveCustomerStatistics(CUSTOMER_ID);
        LoanStatisticsResponse response = responseEntity.getBody();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertThat(Objects.requireNonNull(response).getAvgAmount(), equalTo(BigDecimal.valueOf(125)));
        assertThat(response.getContractsCount(), equalTo(2));
        assertThat(response.getSumOfAmounts(), equalTo(BigDecimal.valueOf(250)));
    }

}
