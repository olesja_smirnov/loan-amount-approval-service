package laas;

import static laas.ValidatorUtils.validate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.regex.Matcher;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationTest {

    @Test
    public void validateCustomerId() {
        Matcher matcherTrue = validate("d2-d1s-tok");
        assertTrue(matcherTrue.matches());

        Matcher matcherFalse = validate("e8cc6aba-2915-4b4d-afa8-7ed538532e65");
        assertFalse(matcherFalse.matches());
    }

}