package laas.domain.request;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerApplicationRequest {

    @NotNull
    private String customerId;

    @NotNull
    private BigDecimal loanAmount;

    @NotNull
    @Size(min=1, max=3, message = "There must be 1-3 managers")
    private List<String> usernames;

}