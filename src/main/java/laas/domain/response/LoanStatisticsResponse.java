package laas.domain.response;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanStatisticsResponse {

    private int contractsCount;

    private BigDecimal sumOfAmounts;

    private BigDecimal avgAmount;

    private BigDecimal maxAmount;

    private BigDecimal minAmount;

}