package laas.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import laas.domain.request.CustomerApplicationRequest;
import laas.domain.response.LoanStatisticsResponse;
import laas.errors.GenericException;
import laas.errors.ResourceNotFoundException;
import laas.model.Contract;
import laas.model.Customer;
import laas.model.Manager;

@Service
@Slf4j
public class LoanService {

    private static final List<Customer> CUSTOMERS = new ArrayList<>();

    public String proceedCustomerRequest(CustomerApplicationRequest request) {

        Optional<Customer> customerOptional = getCustomerById(request.getCustomerId());
        if (customerOptional.isEmpty()) {
            createNewCustomer(request);
            return "Request is received for new Customer with ID " + request.getCustomerId();
        }

        Customer customer = customerOptional.get();

        boolean isTherePendingRequests = checkForPendingLoanRequest(customer);
        if (isTherePendingRequests) {
            throw new GenericException("There is a pending Request for customer ID " + request.getCustomerId() + ".");
        }

        customer.getContracts().add(createNewContract(request.getLoanAmount()));
        customer.setPendingRequest(true);
        customer.setManagers(getManagersForCustomer(request.getUsernames()));
        log.info("Managers for existing customer are: " + request.getUsernames());

        return "Request is received for Customer with ID " + request.getCustomerId() + " and processing.";
    }

    public String getManagerApproval(String customerId, String username) {

        Optional<Customer> customerOptional = getCustomerById(customerId);
        if (customerOptional.isEmpty()) {
            throw new ResourceNotFoundException("There is no Customer with ID " + customerId);
        }

        Customer customer = customerOptional.get();

        List<String> usernames = getUsernamesFromCustomerRequest(customer.getManagers());
        if (usernames.contains(username)) {
            markManagerApproval(customer.getManagers(), username);

            boolean isAllApproved = isAllManagersApproved(customer.getManagers());
            log.info("Managers for approval: " + customer.getManagers());

            if (isAllApproved) {
                Contract notApprovedContract = customer.getContracts().stream()
                        .filter(c -> c.getApprovalSentDateTime() == null)
                        .findAny().orElse(null);
                Objects.requireNonNull(notApprovedContract).setApprovalSentDateTime(LocalDateTime.now());

                customer.setPendingRequest(false);
                return "Loan is approved by all assigned managers: " + usernames;
            }

            return "Loan is approved by " + username;
        }

        return "Manager " + username + " is not assigned to approve loan request for Customer ID " + customerId;
    }

    private void markManagerApproval(List<Manager> managers, String username) {
        Manager manager = managers.stream()
                .filter(m -> m.getUsername().equals(username))
                .findAny().orElse(null);
        Objects.requireNonNull(manager).setIsApproved(true);
    }

    public boolean isAllManagersApproved(List<Manager> managers) {
        Optional<Manager> unApproved = managers.stream()
                .filter(m -> !m.getIsApproved())
                .findAny();

        return unApproved.isEmpty();
    }

    public List<String> getUsernamesFromCustomerRequest(List<Manager> managers) {

        return managers.stream()
                .map(Manager::getUsername)
                .collect(Collectors.toList());
    }

    public void createNewCustomer(CustomerApplicationRequest request) {
        List<Contract> contracts = new ArrayList<>();
        contracts.add(createNewContract(request.getLoanAmount()));

        List<Manager> managers = getManagersForCustomer(request.getUsernames());

        Customer newCustomer = Customer.builder()
                .id(request.getCustomerId())
                .pendingRequest(true)
                .contracts(contracts)
                .managers(managers)
                .build();
        CUSTOMERS.add(newCustomer);
    }

    public Optional<Customer> getCustomerById(String customerId) {

        return CUSTOMERS.stream()
                .filter(c -> c.getId().equals(customerId))
                .findFirst();
    }

    public boolean checkForPendingLoanRequest(Customer customer) {

        return customer.getPendingRequest();
    }

    public Contract createNewContract(BigDecimal loanAmount) {

        return Contract.builder()
                .loanAmount(loanAmount)
                .build();
    }

    public List<Manager> getManagersForCustomer(List<String> usernames) {

        return usernames.stream()
                .map(username -> Manager.builder()
                        .username(username)
                        .build()).collect(Collectors.toList());
    }

    public LoanStatisticsResponse getCustomerLoanStatistics(String customerId) {
        Optional<Customer> customerOptional = getCustomerById(customerId);
        if (customerOptional.isEmpty()) {
            throw new ResourceNotFoundException("There is no Customer with ID " + customerId);
        }

        Customer customer = customerOptional.get();

        List<Contract> filteredContracts = getRecentlyApprovedContracts(customer.getContracts());
        if (filteredContracts.isEmpty()) {
            log.info("There is no approved loan requests within last 60 seconds.");
            return null;
        }

        BigDecimal sumOfAmounts = BigDecimal.ZERO;
        BigDecimal max = BigDecimal.valueOf(Integer.MIN_VALUE);
        BigDecimal min = BigDecimal.valueOf(Integer.MAX_VALUE);

        for (Contract contract : filteredContracts) {
            sumOfAmounts = sumOfAmounts.add(contract.getLoanAmount());
            // have chosen quick sort at least here for better performance, as required in a task :))))))) other functionality of service - stream()  :)
            // most WEB Applications using streams for better readability
            if (contract.getLoanAmount().compareTo(min) < 0) {
                min = contract.getLoanAmount();
            }
            if (contract.getLoanAmount().compareTo(max) > 0) {
                max = contract.getLoanAmount();
            }
        }
        BigDecimal avgAmount = sumOfAmounts.divide(BigDecimal.valueOf(filteredContracts.size()), RoundingMode.HALF_UP);

        return getLoanStatisticsResponseBuilder(filteredContracts.size(), sumOfAmounts, avgAmount, max, min);
    }

    public List<Contract> getRecentlyApprovedContracts(List<Contract> contracts) { // Approved contracts within last 60 seconds

        return contracts.stream()
                .filter(c -> c.getApprovalSentDateTime() != null &&
                        c.getApprovalSentDateTime().isAfter(LocalDateTime.now().minusSeconds(60)))
                .collect(Collectors.toList());
    }

    private LoanStatisticsResponse getLoanStatisticsResponseBuilder(Integer count, BigDecimal sum,
                                                                    BigDecimal avg, BigDecimal max,
                                                                    BigDecimal min) {
        return LoanStatisticsResponse.builder()
                .contractsCount(count)
                .sumOfAmounts(sum)
                .avgAmount(avg)
                .maxAmount(max)
                .minAmount(min)
                .build();
    }

}