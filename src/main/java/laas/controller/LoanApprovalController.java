package laas.controller;

import static laas.ValidatorUtils.validate;

import java.util.regex.Matcher;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import laas.domain.request.CustomerApplicationRequest;
import laas.domain.request.ManagerApprovalRequest;
import laas.domain.response.LoanStatisticsResponse;
import laas.errors.ValidationException;
import laas.service.LoanService;

@RestController
@RequestMapping(path = "/api/loan")
@Api(tags = "loan", value = "/api/loan")
@Validated
public class LoanApprovalController {

    private final LoanService loanService;

    public LoanApprovalController(LoanService loanService) {
        this.loanService = loanService;
    }

    @ApiOperation("Customer's request for loan approval")
    @PostMapping("/request")
    public ResponseEntity<String> approveCustomerLoanRequest(@RequestBody @Valid CustomerApplicationRequest request) {
        Matcher m = validate(request.getCustomerId());
        if (!m.matches()) {
            throw new ValidationException("Customer ID must be in a pattern XX-XXXX-XXX where X is either number or a letter");
        }

        return ResponseEntity.ok(loanService.proceedCustomerRequest(request));
    }

    @ApiOperation("Send request to Managers for approval")
    @PostMapping("/approval")
    public ResponseEntity<String> sendRequestToManagers(@RequestBody @Valid ManagerApprovalRequest request) {
        Matcher m = validate(request.getCustomerId());
        if (!m.matches()) {
            throw new ValidationException("Customer ID must be in a pattern XX-XXXX-XXX where X is either number or a letter");
        }

        return ResponseEntity.ok(loanService.getManagerApproval(request.getCustomerId(), request.getUsername()));
    }

    @ApiOperation("Get Customer's statistics")
    @GetMapping("/statistics")
    public ResponseEntity<LoanStatisticsResponse> receiveCustomerStatistics(@RequestParam @Valid String customerId) {
        Matcher m = validate(customerId);
        if (!m.matches()) {
            throw new ValidationException("Customer ID must be in a pattern XX-XXXX-XXX where X is either number or a letter");
        }

        return ResponseEntity.ok(loanService.getCustomerLoanStatistics(customerId));
    }

}