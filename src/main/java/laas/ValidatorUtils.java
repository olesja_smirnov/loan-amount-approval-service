package laas;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ValidatorUtils {

    private ValidatorUtils() {
    }

    public static Matcher validate(String customerId) {
        String regex = "\\p{Alnum}{2}-\\p{Alnum}{3}-\\p{Alnum}{3}";
        Pattern p = Pattern.compile(regex);

        return p.matcher(customerId);
    }

}
