package laas.model;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Manager {

    @NotNull
    private String username;

    @NotNull
    @Builder.Default
    private Boolean isApproved = false;

}