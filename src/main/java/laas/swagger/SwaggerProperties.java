package laas.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "java.demo.swagger")

public class SwaggerProperties {
    private String group;
    private String title;
    private String description;
    private String version;
}