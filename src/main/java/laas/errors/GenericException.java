package laas.errors;

public class GenericException extends RuntimeException {

    public GenericException(String message) {
        super(message);
    }

}